<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use wadeshuler\sliderrevolution\SliderRevolution;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>This is the About page. You may modify the following file to customize its content:</p>

    <code><?= __FILE__ ?></code>
</div>

<?php
$config = ['delay' => 9000, 'startwidth' => 1170, 'startheight' => 500, 'hideThumbs' => 10, 'fullWidth' => '"on"', 'forceFullWidth' => '"on"'];
$container = ['class' => 'tp-banner-container'];
$wrapper = ['class' => 'tp-banner'];
$ulOptions = ['class' => 'my-ul'];

$slides = [
    [
        'options' => ['data' => ['transition' => 'fade', 'slotamount' => '2', 'masterspeed' => '1500']],
        'image' => ['src' => Yii::$app->params['baseUrl'] . 'frontend/web/images/images1.png', 'options' => ['alt' => 'slidebg1', 'data' => ['bgfit' => 'cover', 'bgposition' => 'left center', 'bgrepeat' => 'no-repeat']]],
        'layers' => [
            [
                'options' => ['class' => 'tp-caption lft', 'data' => ['x' => 'center', 'y' => 'top', 'hoffset' => '0', 'voffset' => '50', 'speed' => '2500', 'start' => '1200', 'easing' => 'Power4.easeOut', 'endspeed' => '300', 'endeasing' => 'Power1.easeIn', 'captionhidden' => 'off'], 'style' => 'z-index: 6'],
                'content' => 'My Slide'
            ],
            [
                'options' => ['class' => 'tp-caption lfr', 'data' => ['x' => 'center', 'y' => 'bottom', 'hoffset' => '0', 'voffset' => '-50', 'speed' => '2500', 'start' => '1800', 'easing' => 'Power4.easeOut', 'endspeed' => '300', 'endeasing' => 'Power1.easeIn', 'captionhidden' => 'off'], 'style' => 'z-index: 6'],
                'content' => 'My Text'
            ],
        ],
    ]
];

echo SliderRevolution::widget([
    'config' => $config,
    'container' => $container,
    'wrapper' => $wrapper,
    'ulOptions' => $ulOptions,
    'slides' => $slides
]);
?>